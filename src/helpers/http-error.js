const {
    UniqueConstraintError,
    InvalidPropertyError,
    RequiredParameterError,
    PasswordOrMailError,
    NotFoundError,
    ValidationError
} = require("../helpers/errors");

function makeHttpError(error) {
    let statusCode;
    let errorMessage = error.message || "Error message.";
    if (error instanceof UniqueConstraintError) statusCode = 409
    else if (error instanceof InvalidPropertyError || error instanceof RequiredParameterError) statusCode = 400
    else if (error instanceof PasswordOrMailError) statusCode = 401
    else if (error instanceof ValidationError) statusCode = 400
    else if (error instanceof NotFoundError) statusCode = 404

    return {
        headers: {
            'Content-Type': 'application/json'
        },
        statusCode,
        data: JSON.stringify({
            success: false,
            error: errorMessage
        })
    }
}

module.exports = {
    makeHttpError,
}
