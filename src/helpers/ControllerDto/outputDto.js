module.exports = {
    outputDto,
}

/**
 *@outputDto Data Transfer and validation
 * @outputDtoDescription creation of output obj that is appropriate for outer framework(i.e. React.js)
 *@outputDtoParam {object}
 *outputDtoSuccess {object}
 */

function outputDto(response) {
    //TODO edit and manipulate the "response" data too!
    return {
        headers: {
            'Content-Type': 'application/json'
        },
        statusCode: 201,
        data: JSON.stringify(response)
    }
}