const {PropertyRequiredError} = require("../helpers/errors");

function requiredParam(param) {
    let err = new PropertyRequiredError(param);
    Error.captureStackTrace(err, requiredParam);
    throw  err;
}

module.exports = {
    requiredParam,
}
