function upperFirst(words) {
    let firstUpper = words.map(word => {
        word.trim();
        return word.length === 1 ? word.toUpperCase() : word.charAt(0).toUpperCase() + word.substring(1)
    })
    return firstUpper;
}

module.exports = {
    upperFirst,
}
