

function DbModelConstructor(mongoosoe) {
    const schema_constructor = ((mongoose) => {
        const definition = {
            name: {
                type: String,
                required: true
            },
            surname: {
                type: String,
                required: true
            },
            pwd: {
                type: String,
                required: true
            },
            mail: {
                type: String,
                unique: true,
                required: true,
                trim: true
            },
            refresh_token: {
                type: String,
                default: null
            },
            type: {
                type: String,
                default: 'customer'
            },
            failedCount: {
                type: Number,
                default: 0
            },
            accessAllowed: {
                type: Boolean,
                default: true
            },
            //TODO add compare password to the user Schema, namely to the schema inside the constructor!
        };
        return () => new mongoose.Schema(definition);
    })(mongoosoe);



    class CustomerDBModel {
        constructor() {
            this.schema = schema_constructor();
            this.model = mongoosoe.model("Customer", this.schema, "customers");
        }
    }



    return new CustomerDBModel();
}


module.exports = {
    DbModelConstructor,
}