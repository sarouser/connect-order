//     // const usersRouter = require("./routes/user.routes");
// // app.use("/users", usersRouter);
// const {logger} = require("sequelize");
//

// const bcrypt = require("bcrypt"),
//     SALT_WORK_FACTOR = 11;
//
//
//
// const specialist = new Schema({
//     name: {
//         type: String,
//         trim: true
//     },
//     surname: {
//         type: String,
//         trim: true
//     },
//     prof_pic: String,
//     pwd: String,
//     mail: {
//         type: String,
//         unique: true,
//         trim: true,
//         match: "/^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$/",
//         maxlength: 320
//     },
//     rating: Number,
//     refresh_token: String
// });
//
//
// const services = new Schema({
//     name: String,
//     description: String,
//     hRate: Number,
//     pref_work_hours: Date,
//     owner_id: mongoose.ObjectId
// });
// const projects = new Schema({
//     started: Boolean,
//     finished: Boolean,
//     accepted: Boolean,
//     service_id: mongoose.ObjectId,
//     customer_id: mongoose.ObjectId
// });
//
//
// const projects_model = mongoose.model('Project', projects, "projects");
// const service_model = mongoose.model("Service", services, "services");
// // exports.
// const Project = projects_model;
// const Customer = customer_model;
// const Service = service_model;
//
// module.exports = {
//     Project,
//     Customer,
//     Service,
// }


const {Customer} = require("./CustomerDBModel");
//This part can be perceived as an interface for customer_methods
//though, not really an interface, but6 the names will
//still be the same, so it can be taken into consideration as an interface!
//TODO create login and update

// async function update(info, checkedAuth) {
//     try {
//         let res = await customer_model.findOneAndUpdate({_id: checkedAuth.id}, info,
//             {returnOriginal: false});
//         return send_message().updated();
//     } catch (e) {
//         throw new NotFoundError();
//     }
// }
module.exports = {
    customer_methods,
}

function customer_methods() {
    // async function login(info) {
    //     //TODO change lookup subject to _id instead of mail!
    //     const res = await customer_model.findOne({mail: info.mail})
    //         .catch(err => {
    //             console.log("ERR of findOne", err)
    //             throw new PasswordOrMailError();
    //         });
    //
    //
    //     if (res) {
    //         if (await compare_pwds(info.pwd, res.pwd)) {
    //             let response = {type: res.type, mail: res.mail, id: res._id};
    //             console.log(res._id);
    //             //Generate jwts
    //             response = Object.assign(await promisified_jwt_sign(response));
    //             return response;
    //         }
    //     } else throw new PasswordOrMailError();
    // }

    //get customer model obj, and then save that to DB
    async function add(customer) {
        // try {
            const {_doc} = await customer.save();
            return _doc;
        // } catch (err) {
        //     throw new Error(err._message || err.message || "Err in CustomerDB!");
        //     // if (err.name === 'MongoError' && err.code === 11000) {
        //     //     throw new UniqueConstraintError(Object.keys(err.keyPattern)[0], true);
        //     // } else if (err.name === "ValidationError") {
        //     //     throw  new ValidationError(err._message)
        //     // }
        // }
        //TODO change below message to obj(signed_up message)!
    }

    return Object.freeze({
        add,
    })
}