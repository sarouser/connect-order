// [-] model of customer
const mongoose = require("mongoose");
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
    poolSize: 10, // Maintain up to 10 socket connections
    serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
    // socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    family: 4 // Use IPv4, skip trying IPv6
};
// mongoose.connect(url, options);
const mongodb = require("mongodb");

async function makeDb() {
    const MongoClient = mongodb.MongoClient
    const dbName = 'cs-connect';
    const url = `mongodb://localhost:27017/${dbName}`;
   mongoose.connect(url,options,function (on) {
        console.log("on",on)
    })
    mongoose.Promise = global.Promise;

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        // we're connected!
        console.log("connected");
    });
    // const client = new MongoClient(url, {useNewUrlParser: true, useUnifiedTopology: true})
    // await client.connect()

    // await client.db().collections();
    // console.log("collections->",client.db().collections().then((list)=>console.log("list",list)));
    //console.log("list",client.db().admin().listDatabases().then((list)=>console.log("list",list)));
    // const db = await client.db(dbName)
    // db.makeId = makeIdFromString
    return db
}

function makeIdFromString(id) {
    return new mongodb.ObjectID(id)
}

module.exports = {
    makeDb,
}


// console.log("global")
// console.log(global)
// const express = require("express");
// const cors = require("cors");
// const mongoose = require("mongoose");
// const bodyParser = require('body-parser');
// const {MongoClient} = require('mongodb');
// // const uri = 'mongodb+srv://sarouser:QRTS2azgiimongo@cluster0.2rsox.mongodb.net/test';
// const uri = 'mongodb://localhost:27017/cs-connect';
// // const client = new MongoClient(uri);
// const db_model = require("./models/db.model");
// require("dotenv").config();
// const {Schema} = mongoose;
// const {setupRoutes} = require("./routes/setupRoutes");
// const app = express();
// const port = process.env.PORT || 5000;
//
// app.use(cors());
// app.use(express.json());
// app.use(bodyParser.urlencoded({
//     extended: true
// }));
//
// const server = '127.0.0.1:27017'; // REPLACE WITH YOUR DB SERVER
// const database = 'cs-connect';      // REPLACE WITH YOUR DB NAME
// mongoose.connect(`mongodb://${server}/${database}`, {
//     useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false,
//     useCreateIndex: true
// })
//     .then(() => {
//         console.log('Database connection successful');
//     })
//     .catch(err => {
//         console.error('Database connection error');
//         console.log(err);
//     })
//
// app.use(setupRoutes());
//
// app.listen(port, () => {
//     console.log(`Server is running on port: ${port}`);
// })
