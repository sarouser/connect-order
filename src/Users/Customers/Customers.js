const {requiredParam} = require("../../helpers/required-param");
const {validatorLogic} = require("./Validator");
const {isValidEmail} = require("../../helpers/is-valid-email");
const {upperFirst} = require("../../helpers/upper-first");
const {InvalidPropertyError, PasswordOrMailError} = require("../../helpers/errors");


function CustomerModel({
                           bcrypt: bcrypt = requiredParam("cryptography library is missing!"),
                           SALT_WORK_FACTOR: SALT_WORK_FACTOR = requiredParam("SALT_WORK_FACTOR is missing!")
                       }) {
    const validationHandler = validatorLogic(bcrypt);

    // const {bcrypt, SALT_WORK_FACTOR} = options;

    class CustomerClass {
        constructor(properties = requiredParam("Invalid customer properties!")) {
            Object.assign(this, properties);
        }

        async hashPassword() {
            let salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
            this.pwd = await bcrypt.hash(this.pwd, salt);

        }

    }

    const Customer = new Proxy(CustomerClass, validationHandler);

    // console.log("SALT_WORK_FACTOR", SALT_WORK_FACTOR)
    // console.log("bcrypt", bcrypt)
    // console.log("dependencies", options)

    //Name and Surname first letter upper case
    //TODO remove unnecessary function due to code refactoring[-]
    function capitalizeFirst(next) {
        // capitalize
        this.name = this.name.charAt(0).toUpperCase() + this.name.slice(1).toLowerCase();
        this.surname = this.surname.charAt(0).toUpperCase() + this.surname.slice(1).toLowerCase();
        next();
    }


    //TODO add const sanitizeHtml = require("sanitize-html") in inp_constructor


    return Customer;

}

function CustomDataConstructor(data) {
    this.name = data.name;
    this.surname = data.surname;
    this.pwd = data.pwd;
    this.mail = data.mail;
};
module.exports = {
    CustomDataConstructor, CustomerModel,
}
