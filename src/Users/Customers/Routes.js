const routes = {
    // 'POST /login': {
    //     controllerIdentifier: "signInCustomer",
    //     middleware: false,
    // },
    // 'POST /user': {
    //     controllerIdentifier: "updateCustomer",
    //     middleware: true,
    //     middlewareFunctions: ["checkAuth", "errorHandlerMiddleware"]
    // },
    'POST /register': {
        controllerIdentifier: "postCustomer",
        middleware: false
    }
};

function RoutesConstructor(middleware, Controller) {
    for (let key in routes) {
        let current = routes[key];
        current.httpAdapter = middleware.httpAdapter;
        current.controller = Controller[current["controllerIdentifier"]];
        console.log(current.httpAdapter.toString())
    }
    return routes;
}

exports.getRoutes = ({middleware, Controller}) => {
    return RoutesConstructor(middleware, Controller);
}
