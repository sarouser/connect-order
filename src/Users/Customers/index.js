const {makeDb} = require("../../db/index"),
    // {createCustomerList} = require("./Customer-Model"),
    {makeRoutesHandler} = require("../Customers/Customer-Routes"),
    {Middleware_constructor} = require("../../middlewares/index"),
    {Controller_Constructor} = require("../Customers/Customer-Controller"),
    {getRoutes} = require("../Customers/Routes"),
    {CustomerModel} = require("../Customers/Customers"),
    {DbModelConstructor} = require("../../db/CustomerDBModel"),
    {CustomerUseCases} = require("../Customers/UseCases"),
    {CustomerController} = require("../Customers/Customer-Controller"),
    mongoose = require("mongoose"),
    express = require("express"),
    {customer_methods} = require("../../db/CustomerDB"),
    jwt = require("jsonwebtoken"),
    bcrypt = require("bcrypt"),
    // {CustomDataConstructor, CustomerModel} = require("../Customers/Customers"),
    {private_key_access, private_key_refresh, SALT_WORK_FACTOR} = require("../../../config"),
    // customer_model = CustomerModel({bcrypt}),
    jwt_config = {private_key_access, private_key_refresh, jwt};


//3rd level -> Create methods for connecting to DB, namely here we have customer_methods for connecting to Db and making changes
//to our desired documents(tables)

//1st level -> Construct Customer(DS) and call DB functions by invoking one of the customer_methods!

//2nd level -> Handle controller, in other words, the ControllerDto res part and the overall bridge between business logic and first layer of
//the program, namely the ControllerDto res handler, the way our business logic gets the data and the way an end user gets response
//are both defined here! P.S. meaning here it can only call particular functions, instead of operating hard and business
//related logic!

//1-makedDB
//2-middleware
//3-Customer Model "the DS part"
//4-CustomerDB or "customer_methods"
//5-UseCase "the way our App logic will interact with 3 and 4"
//6-controller "ControllerDto res handler"

//

const database = makeDb();

const middleware = Middleware_constructor({jwt});
const Customer = CustomerModel({bcrypt, SALT_WORK_FACTOR});
const {model} = DbModelConstructor(mongoose);
const methods = customer_methods();
const useCases = CustomerUseCases(model, methods, Customer);
const Controller = CustomerController(useCases);

const props = {middleware, Controller};
// const properties = {database, jwt, CustomDataConstructor, CustomerModel, bcrypt, jwt_config, SALT_WORK_FACTOR};
// const controller = Controller_Constructor(properties);
const routes = getRoutes(props);
//routes constructor

const handleCustomerRequests = makeRoutesHandler({middleware, routes, Controller, express});

module.exports = {handleCustomerRequests};
