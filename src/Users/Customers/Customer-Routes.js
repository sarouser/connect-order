//TODO this is setupRoutes js script
const {CustomerModel} = require("../Customers/Customers");
// const {customerList} = require("../Customers/Customers-list");
const {makeHttpError} = require("../../helpers/http-error");
const SALT_WORK_FACTOR = 11;
const {UniqueConstraintError, InvalidPropertyError, RequiredParameterError} = require("../../helpers/errors"),
    bcrypt = require("bcrypt");
//model of customer entity
// {functionalities, customer_model} = CustomerModel({bcrypt, SALT_WORK_FACTOR});
// console.log("Constructor", CustomerModel);
// console.log("initial", customer_model);
// console.log("curtomer_model", customer_model)
// Constructor = customer_model.inp_constructor;


function makeRoutesHandler({middleware, routes, express, controller}) {
    const router = setupRoutes(routes);
    return router;

    //TODO add next to middlewares

    // const middlewareFunctions = middleware;

    function setupRoutes(routes) {
        const router = express.Router();
        // load all internal routes
        const routesKey = Object.keys(routes);
        routesKey.forEach((routeKey) => {
            const splitted = routeKey.split(' ');
            const method = splitted[0];
            const path = splitted[1];
            const routerParams = [];
            let current = routes[routeKey];
            // if the route is marked as authenticated
            routerParams.push(current.httpAdapter);
            if (current.middleware) {
                current.middlewareFunctions.forEach(func => {
                    routerParams.push(middleware[func])
                    console.log("func")
                    console.log(middleware[func])
                    console.log("logggggggg", func)
                })
                // routerParams.push(current.middlewareFunctions);
            }
            // add the controller at the end of the array

            routerParams.push(current.controller);

            // call the router with the params
            // equivalent to router.method(path , params) [params = middleware + handler]
            router[method.toLowerCase()](path, ...routerParams);
        });

        return router;
    }


    // return async function (httpRequest) {
    //     const {method, path, body, headers} = httpRequest;
    //     let router_path = router[`${method} ${[path]}`];
    //     if (router_path) {
    //         if (router_path.middleware) {
    //             //TODO Do middleware checks
    //         }
    //         return await router_path.routeHandler(httpRequest);
    //     } else return makeHttpError({
    //         statusCode: 405,
    //         errorMessage: `${method} method not allowed.`
    //     })
    // }

    // function setupRoutes() {
    //     const router = {}; //{}
    //
    //     // enable cross origin requests
    //
    // const routes = {
    //     'POST /login': {
    //         middleware: false,
    //         controller: signInCustomer,
    //     },
    //     'POST /user': {
    //         middleware: true,
    //         controller: updateCustomer,
    //     }
    // };
    //
    //     // load all internal routes
    //     const routesKey = Object.keys(routes);
    //
    //     routesKey.forEach((routeKey) => {
    //         const splitted = routeKey.split(' ');
    //         const method = splitted[0];
    //         const path = splitted[1];
    //         const routeHandler = routes[routeKey].controller;
    //         const routeMiddlewares = [];
    //         let flag = routes[routeKey].middleware;
    //         // if the route is marked as middleware
    //         if (flag) {
    //             Object.keys(routes[routeKey].middleware_functions)
    //                 .forEach(func => routeMiddlewares.push(routes[routeKey].middleware_functions[func])); //authMiddleware is a function!
    //         }
    //         // add the controller at the end of the array
    //         // routeHandler.push(routes[routeKey].controller);
    //         //Set (method path) property to the object router
    //         router[`${method} ${path}`] = {
    //             path, routeHandler, routeMiddlewares
    //         };
    //         // equivalent to router.method(path , params) [params = middleware + handler]
    //         // router[method](path, ...routeHandler);
    //     });
    //
    //
    //     return router;
    //
    // }


    //TODO here call middlewares, then call base functions![current]
    // return async function handle(httpRequest) {
    //     let res = await middleware(httpRequest);
    //
    //
    //     //TODO Separate the whole switch logic
    //     switch (httpRequest.method) {
    //         case 'POST':
    //             return await signInCustomer(httpRequest);
    //         // Initially at 22:- return await postCustomer(httpRequest);
    //         case 'PUT':
    //             return updateCustomer(httpRequest);
    //         // case 'GET':
    //         //     return getContacts(httpRequest)
    //
    //         default:
    //             return makeHttpError({
    //                 statusCode: 405,
    //                 errorMessage: `${httpRequest.method} method not allowed.`
    //             })
    //     }
    //
    //
    // }

    //TODO remove from here!


    // function checker(httpRequest) {
    //     let info = httpRequest.body;
    //     console.log(info);
    //
    //     if (!info) {
    //         return makeHttpError({
    //             statusCode: 400,
    //             errorMessage: 'Bad request. No POST body.'
    //         })
    //     }
    //
    //     if (typeof httpRequest.body === 'string') {
    //         try {
    //             info = JSON.parse(info)
    //         } catch {
    //             return makeHttpError({
    //                 statusCode: 400,
    //                 errorMessage: 'Bad request. POST body must be valid JSON.'
    //             })
    //         }
    //     }
    //     return info;
    // }

    //TODO signInCustomer, add JWT generation, then mark it done!
    // async function signInCustomer(ControllerDto, res, next) {
    //     let httpRequest =
    //         console.log("customer_model", customer_model)
    //     let info = checker(httpRequest);
    //     try {
    //         const input = {info, customer_model, compare_pwds: functionalities.compare_pwds};
    //
    //         const result = await customerList.login(input);
    //         //TODO add jwt generating functions and return those to clients!
    //         return {
    //             headers: {
    //                 'Content-Type': 'application/json'
    //             },
    //             statusCode: 201,
    //             data: JSON.stringify(result)
    //         }
    //     } catch (e) {
    //         //TODO write separate makeHttpError function which includes all logic for def. message and statusCode!
    //         return makeHttpError({
    //             errorMessage: e.message,
    //             statusCode:
    //                 e instanceof UniqueConstraintError
    //                     ? 409
    //                     : e instanceof InvalidPropertyError ||
    //                     e instanceof RequiredParameterError
    //                     ? 400
    //                     : 500
    //         })
    //     }
    //
    // }

    // async function updateCustomer(httpRequest) {
    //     let info = checker(httpRequest);
    //     const result = customerList.update(info);
    //
    // }

    // async function postCustomer(httpRequest) {
    //     let info = checker(httpRequest);
    //     // let info = httpRequest.body
    //     // console.log(info)
    //     //
    //     // if (!info) {
    //     //     return makeHttpError({
    //     //         statusCode: 400,
    //     //         errorMessage: 'Bad request. No POST body.'
    //     //     })
    //     // }
    //     //
    //     // if (typeof httpRequest.body === 'string') {
    //     //     try {
    //     //         info = JSON.parse(info)
    //     //     } catch {
    //     //         return makeHttpError({
    //     //             statusCode: 400,
    //     //             errorMessage: 'Bad request. POST body must be valid JSON.'
    //     //         })
    //     //     }
    //     // }
    //
    //     try {
    //         // const res = await hanldePostAsync();
    //
    //         const result = await customerList.add(info);
    //
    //         return {
    //             headers: {
    //                 'Content-Type': 'application/json'
    //             },
    //             statusCode: 201,
    //             data: JSON.stringify(result)
    //         }
    //
    //     } catch (e) {
    //         return makeHttpError({
    //             errorMessage: e.message,
    //             statusCode:
    //                 e instanceof UniqueConstraintError
    //                     ? 409
    //                     : e instanceof InvalidPropertyError ||
    //                     e instanceof RequiredParameterError
    //                     ? 400
    //                     : 500
    //         })
    //     }
    // }

}

module.exports = {
    makeRoutesHandler,
}
