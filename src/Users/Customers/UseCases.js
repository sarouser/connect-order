function CustomerUseCases(model, customer_methods, customer_model) {
    async function createCustomer(info) {

        let created_customer = new customer_model(info);
        await created_customer.hashPassword();
        let model_instance = new model(created_customer);
        const result = await customer_methods.add(model_instance);
        return result;
    }

    return Object.freeze({
        createCustomer
    })

}

module.exports = {
    CustomerUseCases,
}
