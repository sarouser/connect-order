const {getFunctions} = require("./functions");
// exports.Middleware = {
//     checkAuth: (httpRequest) => {
//
//     }
// }

function Middleware_constructor(obj) {
    const middleware_funcs = getFunctions(obj);
    return middleware_funcs;
}

module.exports = {Middleware_constructor};
