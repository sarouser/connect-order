//TODO add unit tests
'use strict';
// import {afterAll, afterEach, beforeAll, describe, it} from "@jest/globals";

const {makeDb} = require("../src/db/index"),
    {CustomerModel, CustomDataConstructor} = require("../src/Users/Customers/Customers"),
    {createCustomerList} = require("../src/Users/Customers/Customer-Model"),
    bcrypt = require("bcrypt"),
    // customer_model = CustomerModel({bcrypt}),
    mongoose = require("mongoose"),
    dbHandler = require('./db-handler');


const CustomerData = {name: "palo", surname: "maloqo", mail: "kaloa@gmail.com", pwd: "qwerty"};
// const database = makeDb();
// const customerModel = CustomerModel(bcrypt, SALT_WORK_FACTOR);

// const customerList = createCustomerList(database, {CustomDataConstructor, customer_model}, bcrypt, {});
// describe("Create a customer" , () => {
//
// })


/**
 * Connect to a new in-memory database before running any tests.
 */
beforeAll(async () => {
    await dbHandler.connect();
});

/**
 * Clear all test data after every test.
 */
afterEach(async () => {
    await dbHandler.clearDatabase();
});

/**
 * Remove and close the db and server.
 */
afterAll(async () => {
    await dbHandler.closeDatabase();
});


describe('User Model Test', () => {
    const bcrypt = require("bcrypt");
    const model = CustomerModel({bcrypt});

    it("Create db with \"handler\" function", async () => {
        const db = await dbHandler.connect();
        expect(db.connection.readyState).toBe(1);
    });

    it('test of CustomDataConstructor', async function () {
        let props = {
            name: "alo",
            surname: "blo",
            mail: "mail21a@gmail.com",
            pwd: "qwerty"
        }
        console.log("CustomDataConstructor", CustomDataConstructor)
        let data = new CustomDataConstructor(props)
        console.log("data -> ", data)
        let customer = new model(data);
        let response;
        try {
            response = await customer.save()
        } catch (err) {
            console.log("error occured during the unit testing!")
            console.log(err);
        }
        expect(response).toBeDefined();
        expect(response._id).toBeDefined();
        expect(response.pwd).not.toEqual(data.pwd);
        expect(response.surname).toEqual(data.surname.charAt(0).toUpperCase() + data.surname.slice(1));
        expect(response.name).toEqual(data.name.charAt(0).toUpperCase() + data.name.slice(1));
        expect(response.mail).toEqual(data.mail);
    });

    it('should be built correctly!', async function () {
        let customer = new model(CustomerData);
        let instance;
        try {
            instance = await customer.save()

        } catch (err) {
            console.log("error happened during unit test@")
            console.log(err);
        }
        expect(instance).toBeDefined();
        expect(instance._id).toBeDefined();
        expect(instance.pwd).not.toEqual(CustomerData.pwd);
        expect(instance.surname).toEqual(CustomerData.surname.charAt(0).toUpperCase() + CustomerData.surname.slice(1));
        expect(instance.name).toEqual(CustomerData.name.charAt(0).toUpperCase() + CustomerData.name.slice(1));
        expect(instance.mail).toEqual(CustomerData.mail);

    });

    it('should throw err!', async function () {
        let customer = new model();
        let error;
        try {
            let res = await customer.save();
        } catch (err) {
            error = err;
        }

        expect(error).toBeInstanceOf(mongoose.Error.ValidationError)


    });
});



