//TODO write unit tests!
const {makeDb} = require("../src/db/index"),
    {createCustomerList} = require("../src/Users/Customers/Customer-Model"),
    {Middleware_constructor} = require("../src/middlewares/index"),
    {assert} = require("assert").assert
const database = makeDb();
// const customerModel = CustomerModel(bcrypt, SALT_WORK_FACTOR);
const customerList = createCustomerList(database, {CustomDataConstructor, customer_model}, bcrypt, jwt_config);
const middleware = Middleware_constructor({customerList, jwt});
const {Controller_Constructor} = require("../src/Users/Customers/Customer-Controller");
const functions = Controller_Constructor({customerList});

